#include <timer.h>

#include <Adafruit_NeoPixel.h>

#ifdef __AVR__
#include <avr/power.h>
#endif

#define LILLYPAD

const unsigned NUM_PIXELS = 15;

#if defined(LILLYPAD)
const unsigned BUTTON_BLINKER_RIGHT = 5;
const unsigned BUTTON_BLINKER_LEFT = 2;

const unsigned PIXELS_LEFT = 3;
const unsigned PIXELS_RIGHT = 4;
#else
const unsigned BUTTON_BLINKER_RIGHT = 8;
const unsigned BUTTON_BLINKER_LEFT = 9;
bad
const unsigned PIXELS_LEFT = 2;
const unsigned PIXELS_RIGHT = 3;
#endif

Adafruit_NeoPixel pixels_left = Adafruit_NeoPixel(NUM_PIXELS, PIXELS_LEFT, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel pixels_right = Adafruit_NeoPixel(NUM_PIXELS, PIXELS_RIGHT, NEO_GRB + NEO_KHZ800);

void all_leds_off()
{
  for (unsigned i = 0; i < NUM_PIXELS; i++) {
    pixels_left.setPixelColor(i, pixels_left.Color(0, 0, 0));
    pixels_right.setPixelColor(i, pixels_right.Color(0, 0, 0));
  }
  pixels_left.show();
  pixels_right.show();
}

void do_const_backlight()
{
  for (unsigned i = 0; i < NUM_PIXELS; i++) {
    pixels_left.setPixelColor(i, pixels_left.Color(128, 0, 0));
    pixels_right.setPixelColor(i, pixels_right.Color(128, 0, 0));
  }
  pixels_left.show();
  pixels_right.show();
}

void do_fading_backlight()
{
  int min_level = 5;
  static int brightness = 0, inc = 7;

  brightness = max(min((brightness + inc), 255), min_level);

  if (brightness >= 255) {
    inc = -inc;
  } else if (brightness <= min_level) {
    inc = -inc;
  }

  const float n = NUM_PIXELS / 2.0;
  const float max_scale = 1.2;
  const float step_size = max_scale / n;

  for (unsigned i = 0; i < NUM_PIXELS; i++) {
    float scale;

    if (i <= NUM_PIXELS / 2) {
      scale = (float)i * step_size;
    } else {
      scale = (float)(NUM_PIXELS / 2 - (i - NUM_PIXELS / 2))  * step_size;
    }

    scale *= scale;
    scale = max(0.0, min(scale, 1.0));
    pixels_left.setPixelColor(i, pixels_left.Color(brightness * scale, 0, 0));
    pixels_right.setPixelColor(i, pixels_right.Color(brightness * scale, 0, 0));
  }
  pixels_left.show();
  pixels_right.show();
}

void setup() {
  // put your setup code here, to run once:
  pixels_left.begin();
  pixels_right.begin();

  #if defined(LILLYPAD)
  pinMode(BUTTON_BLINKER_LEFT, INPUT_PULLUP);
  pinMode(BUTTON_BLINKER_RIGHT, INPUT_PULLUP);
  #else
  bad
  pinMode(BUTTON_BLINKER_LEFT, INPUT_PULLUP);
  pinMode(BUTTON_BLINKER_RIGHT, INPUT);
  #endif
  all_leds_off();
}

void do_fading_blink(Adafruit_NeoPixel &pixels, unsigned start_idx, unsigned num_leds, bool right_to_left)
{
  const unsigned BLINK_COL_R = 255;
  const unsigned BLINK_COL_G = 80;
  const unsigned dimm_steps = 16;

  int idx, inc;

  if (right_to_left) {
    inc = -1;
    idx = start_idx + num_leds - 1;
  } else {
    inc = 1;
    idx = start_idx;
  }

  for (unsigned i = 0; i < num_leds; i++) {
    unsigned r = 0, g = 0;
    for (unsigned col_idx = 0; col_idx < dimm_steps; col_idx++) {
      pixels.setPixelColor(idx, pixels.Color(r, g, 0));
      pixels.show();
      r += BLINK_COL_R / dimm_steps;
      g += BLINK_COL_G / dimm_steps;
      delay(50 / dimm_steps);
    }

    idx += inc;
  }

  delay(100);
  all_leds_off();
  delay(100);
}

void do_blink(bool left) {
  all_leds_off();
  if (left) {
    do_fading_blink(pixels_left, 0, NUM_PIXELS, false);
  } else {
    do_fading_blink(pixels_right, 0, NUM_PIXELS, false);
  }
}


bool last_button_left = false, last_button_right = false, last_mode = false;
bool button_left = false, button_right = false, mode = false;

void read_buttons()
{
  const int max_cnt = 6;
  static int left_cnt = 0, right_cnt = 0;

  bool l = !digitalRead(BUTTON_BLINKER_LEFT);
  bool r = !digitalRead(BUTTON_BLINKER_RIGHT);

  if (l) {
    left_cnt = min(left_cnt + 1, max_cnt);
  } else {
    left_cnt = max(left_cnt - 1, 0);
  }

  if (r) {
    right_cnt = min(right_cnt + 1, max_cnt);
  } else {
    right_cnt = max(right_cnt - 1, 0);
  }

  if (r) {
    left_cnt = 0;
    button_left = false;
  } else if (left_cnt >= max_cnt * 3 / 4) {
    button_left = true;
  } else if (left_cnt < max_cnt * 1 / 4) {
    button_left = false;
  }

  if (l) {
    right_cnt = 0;
    button_right = false;
  } if (right_cnt >= max_cnt * 3 / 4) {
    button_right = true;
  } else if (right_cnt < max_cnt * 1 / 4) {
    button_right = false;
  }

  if (l && r) {
    mode = true;
  } else {
    mode = false;
  }
}

void loop() {
  int state = 0;

  unsigned min_blink_cnt = 0;
  int backlight_mode = 0;
  bool blink_left = false;
  bool blink_en = false;

  while (true) {
    delay(20);
 
    if (min_blink_cnt > 0 || blink_en) {
      do_blink(blink_left);
    } else if (backlight_mode == 1) {
      do_const_backlight();
    } else if (backlight_mode == 2) {
      do_fading_backlight();
    } else {
      all_leds_off();
    }

    if (min_blink_cnt > 0) {
      min_blink_cnt--;
    }

    read_buttons();

    if (mode && !last_mode) {
      if (!(last_button_left && last_button_right)) {
        backlight_mode = (backlight_mode + 1) % 3;
      }
    } else if (button_left) {
      if (not last_button_left) {
        min_blink_cnt = 3; // blink minimum 3 times
      }

      blink_en = true;
      blink_left = true;
    } else if (button_right) {
      if (not last_button_right) {
        min_blink_cnt = 3; // blink minimum 3 times
      }

      blink_en = true;
      blink_left = false;
    } else {
      blink_en = false;
    }

    last_button_left = button_left;
    last_button_right = button_right;
    last_mode = mode;
  }
}
